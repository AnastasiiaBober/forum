﻿using Forum.DAL.Interfaces;
using Forum.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forum.DAL.Repositories
{
    public class SectionRepository : ISectionRepository
    {
        readonly ForumContext _context;
        public SectionRepository(ForumContext context)
        {
            _context = context;
        }

        public Section Create(Section item)
        {
            item.Date = DateTime.Now;
            _context.Sections.Add(item);
            return item;
        }

        public void Delete(int id)
        {
            var item = _context.Sections.Find(id);
            if (item != null)
                _context.Sections.Remove(item);
        }

        public Section Get(int id)
        {
            return _context.Sections.Include(x => x.Posts).FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Section> GetAll()
        {
            return _context.Sections.AsNoTracking().OrderBy(x => x.Name).ToList();
        }

        public IEnumerable<Section> GetFilteredByName(string name)
        {
            if (name == null || name == "")
            {
               return GetAll();
            }
            return _context.Sections.AsNoTracking().Where(x => x.Name.StartsWith(name)).OrderBy(x=>x.Name).ToList();
        }

        public void Update(Section item)
        {
            _context.Sections.Update(item);
        }
    }
}
