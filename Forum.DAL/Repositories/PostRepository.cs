﻿using Forum.DAL.Interfaces;
using Forum.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forum.DAL.Repositories
{
    public class PostRepository : IFilteredRepository<Post>
    {
        readonly ForumContext _context;
        public PostRepository(ForumContext context)
        {
            _context = context;
        }
        public Post Create(Post item)
        {
            item.Date = DateTime.Now;
            _context.Posts.Add(item);
            return item;
        }

        public void Delete(int id)
        {
            var item = _context.Posts.Find(id);
            if (item != null)
                _context.Posts.Remove(item);
        }

        public Post Get(int id)
        {
            return _context.Posts.Include(x => x.Photo).Include(x => x.User).FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Post> GetAll()
        {
            return _context.Posts.ToList();
        }

        public IEnumerable<Post> GetFiltered(int offset, int sectionId)
        {
            return _context.Posts.AsNoTracking().Where(x => x.SectionId == sectionId).OrderByDescending(x => x.Date)
                .Skip(offset).Take(6).Include(x => x.Photo).Include(x => x.User).ToList();
        }

        public int GetFilteredCount(int sectionId)
        {
            return _context.Posts.AsNoTracking().Count(x => x.SectionId == sectionId);
        }

        public void Update(Post item)
        {
            _context.Posts.Update(item);
        }
    }
}
