﻿using Forum.DAL.Interfaces;
using Forum.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forum.DAL.Repositories
{
    public class CommentRepository : IFilteredRepository<Comment>
    {
        readonly ForumContext _context;
        public CommentRepository(ForumContext context)
        {
            _context = context;
        }
        public Comment Create(Comment item)
        {
            item.Date = DateTime.Now;
            _context.Comments.Add(item);
            return item;
        }

        public void Delete(int id)
        {
            var comment = _context.Comments.Include(x => x.Photos).FirstOrDefault(x => x.Id == id);
            if (comment != null)
            {
                if (comment.Photos != null && comment.Photos.Any())
                {
                    _context.Photos.RemoveRange(comment.Photos);
                }
                _context.Comments.Remove(comment);
            }
        }

        public Comment Get(int id)
        {
            return _context.Comments.Include(x => x.Photos).Include(x => x.User).FirstOrDefault(x => x.Id == id);
        }

        public IEnumerable<Comment> GetAll()
        {
            return _context.Comments.AsNoTracking().Include(x => x.Photos).Include(x => x.User).ToList();
        }

        public IEnumerable<Comment> GetFiltered(int offset, int postId)
        {
            var comments = _context.Comments.AsNoTracking().Where(x => x.PostId == postId).OrderByDescending(x => x.Date).Skip(offset).Take(6).Include(x => x.Photos).Include(x => x.User).ToList();
            foreach (var item in comments)
            {
                if (item.ParentId != null)
                {
                    item.Parent = Get(item.ParentId.Value) ?? new Comment() { Content = "Deleted" };
                }
            }
            return comments;
        }

        public int GetFilteredCount(int postId)
        {
            return _context.Comments.AsNoTracking().Count(x => x.PostId == postId);
        }

        public void Update(Comment item)
        {
            _context.Comments.Update(item);
        }
    }
}
