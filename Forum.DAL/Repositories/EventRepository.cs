﻿using Forum.DAL.Interfaces;
using Forum.DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forum.DAL.Repositories
{
    public class EventRepository : IEventRepository
    {
        readonly ForumContext _context;
        public EventRepository(ForumContext context)
        {
            _context = context;
        }
        public Event Create(Event item)
        {
            item.Date = DateTime.Now;
            _context.Events.Add(item);
            return item;
        }

        public void Delete(int id)
        {
            var eventfromDB = _context.Events.Find(id);
            _context.Events.Remove(eventfromDB);
        }

        public Event Get(int id)
        {
            return _context.Events.Include(x => x.Photo).FirstOrDefault(x=>x.Id == id);
        }

        public IEnumerable<Event> GetAll()
        {
            return _context.Events.AsNoTracking().Include(x=>x.Photo).ToList();
        }

        public void Update(Event item)
        {
            _context.Events.Update(item);
        }
        public IEnumerable<Event> GetFiltered(int offset)
        {
            return _context.Events.AsNoTracking().OrderByDescending(x => x.Date).Skip(offset).Take(6).Include(x => x.Photo).ToList();
        }

        public int GetFilteredCount()
        {
            return _context.Events.AsNoTracking().Count();
        }
    }
}
