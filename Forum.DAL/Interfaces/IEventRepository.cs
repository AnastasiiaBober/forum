﻿using Forum.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.DAL.Interfaces
{
    public interface IEventRepository : IRepository<Event>
    {
        IEnumerable<Event> GetFiltered(int offset);
        int GetFilteredCount();
    }
}
