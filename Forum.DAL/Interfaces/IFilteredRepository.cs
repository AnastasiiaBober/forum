﻿using Forum.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.DAL.Interfaces
{
    public interface IFilteredRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        IEnumerable<TEntity> GetFiltered(int offset,int Id);
        int GetFilteredCount(int Id);
    }
}
