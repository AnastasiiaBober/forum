﻿using Forum.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.DAL.Interfaces
{
    public interface ISectionRepository : IRepository<Section>
    {
        IEnumerable<Section> GetFilteredByName(string name);
    }
}
