﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.DAL.Models
{
    public class Post : BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public string Address { get; set; }
        public int? PhotoId { get; set; }
        public Photo Photo { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        public Section Section { get; set; }
        public int SectionId { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
