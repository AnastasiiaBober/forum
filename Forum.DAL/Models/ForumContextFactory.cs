﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.DAL.Models
{
    public class ForumContextFactory : IDesignTimeDbContextFactory<ForumContext>
    {
        public ForumContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<ForumContext>();
            optionsBuilder.UseSqlServer("Server=.;Database=Forum;Integrated Security=True;Connection Timeout=30;");

            return new ForumContext(optionsBuilder.Options);
        }
    }
}
