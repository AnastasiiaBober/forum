﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.DAL.Models
{
    public class Comment : BaseEntity
    {
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public int? ParentId { get; set; }
        public Post Post { get; set; }
        public int PostId { get; set; }
        public User User { get; set; }
        public string UserId { get; set; }
        [NotMapped]
        public Comment Parent { get; set; }
        public ICollection<Photo> Photos { get; set; }
    }
}
