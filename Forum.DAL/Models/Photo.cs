﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.DAL.Models
{
    public class Photo : BaseEntity
    {
        public string Code { get; set; }
        public Comment Comment { get; set; }
        public int? CommentId { get; set; }
    }
}
