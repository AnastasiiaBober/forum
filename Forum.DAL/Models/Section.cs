﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.DAL.Models
{
    public class Section : BaseEntity
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public ICollection<Post> Posts { get; set; }
    }
}
