﻿using Forum.DAL.Interfaces;
using Forum.DAL.Models;
using Forum.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.DAL.UOW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly EventRepository _eventRepository;
        private readonly PostRepository _postRepository;
        private readonly SectionRepository _sectionRepository;
        private readonly CommentRepository _commentRepository;
        readonly ForumContext _context;
        public UnitOfWork(ForumContext context)
        {
            _context = context;
        }

        public IFilteredRepository<Comment> CommentRepository => _commentRepository ?? new CommentRepository(_context);

        public IEventRepository EventRepository => _eventRepository ?? new EventRepository(_context);

        public IFilteredRepository<Post> PostRepository => _postRepository ?? new PostRepository(_context);

        public ISectionRepository SectionRepository => _sectionRepository ?? new SectionRepository(_context);

        public void Save()
        {
            _context.SaveChanges();
        }
    }
}
