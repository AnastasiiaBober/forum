﻿using Forum.DAL.Interfaces;
using Forum.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.DAL.UOW
{
    public interface IUnitOfWork
    {
        IFilteredRepository<Comment> CommentRepository { get; }
        IEventRepository EventRepository { get; }
        IFilteredRepository<Post> PostRepository { get; }
        ISectionRepository SectionRepository { get; }
        void Save();
    }
}
