export class ResetPasswordModel {
    email: string;
    code: string;
    password: string;
    constructor(email: string, code: string, password: string) {
        this.email = email;
        this.password = password;
        this.code = code;
    }
}