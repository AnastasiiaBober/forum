export class LoginModel {
    name: string;
    password: string;
    
    constructor(userName: string, password: string) {
        this.name = userName;
        this.password = password;
    }
}