import { CommentModel } from "./comment.model";
import { PhotoModel } from "./event.model";

export class PostModel {
    id: number;
    title: string;
    content: string;
    date: Date;
    address: string;
    photo: PhotoModel;
    userId: string;
    userName: string;
    sectionId: number;
    comments: CommentModel[];
}