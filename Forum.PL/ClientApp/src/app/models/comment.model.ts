import { PhotoModel } from "./event.model";

export class CommentModel {
    id: number;
    content: string;
    date: Date;
    parentId: number;
    userId: string;
    userName: string;
    postId: number;
    photos : PhotoModel[];
    parent: CommentModel;
}