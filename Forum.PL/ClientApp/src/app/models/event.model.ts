export class EventModel {
    id: number;
    title: string;
    content: string;
    date: Date;
    photoId: number;
    photo: PhotoModel;
}

export class PhotoModel {
    code: string
    constructor(code: string) {
        this.code = code;
    }

}