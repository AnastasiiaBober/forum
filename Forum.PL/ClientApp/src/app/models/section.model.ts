import { PostModel } from "./post.model";

export class SectionModel {
    id: number;
    name: string;
    date: Date;
    posts: PostModel[];
}