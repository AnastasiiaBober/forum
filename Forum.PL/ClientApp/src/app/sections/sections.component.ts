import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SectionModel } from '../models';
import { AuthorizationService } from '../services/authorization.service';
import { SectionsService } from '../services/sections.service';

@Component({
  selector: 'app-sections',
  templateUrl: './sections.component.html',
})
export class SectionsComponent implements OnInit {
  public allSections: SectionModel[] = [];
  public sectionsDidLoaded = false;
  isAuthorized = false;
  isAdmin = false;
  public name: string;
  constructor(private sectionService: SectionsService, private authService: AuthorizationService, private router: Router) {

  }
  ngOnInit() {
    this.authService.isAuthorized().subscribe(x => {
      this.isAuthorized = x;
      if (this.isAuthorized) {
        this.authService.checkAdminRole().subscribe(x => this.isAdmin = x);
      }
    });
    this.sectionService.getAll('').subscribe(res => {
      this.allSections = res;
      this.sectionsDidLoaded = true;
    })
  }
  onSearchAction(name: string) {
    this.sectionsDidLoaded = false;
    this.sectionService.getAll(name).subscribe(res => {
      this.allSections = res;
      this.sectionsDidLoaded = true;
    })
  }
  getPosts(id: number) {
    var route = 'sections/' + id;
    this.router.navigate([route]);
  }
  onCreateSection() {
    if (this.name) {
      var newSection = new SectionModel();
      newSection.name = this.name;
      if (this.isAdmin) {
        this.sectionService.sendSection(newSection).subscribe((section: SectionModel) => {
          this.sectionService.getAll('').subscribe(res => {
            this.allSections = res;
            this.sectionsDidLoaded = true;
          })
          this.name = '';
        });
      }
      else {
        this.sectionService.requestNewSection(newSection).subscribe(() => {
          alert("Ваша заявка успешно отправлена и будет рассмотрена в близжайшее время");
          this.name = '';
        });
      }
    }
  }
}
