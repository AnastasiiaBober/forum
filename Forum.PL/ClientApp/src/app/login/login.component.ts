import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ForgotPasswordModel, LoginModel } from '../models';
import { AuthorizationService } from '../services/authorization.service';

@Component({
  selector: 'app-login-component',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
login: string;
password: string;
reset_email: string;
constructor(private service: AuthorizationService,  private router: Router){

}
authorize(){
this.service.authorize(new LoginModel(this.login, this.password)).subscribe(resp =>
  {
    if (resp['status'] === 200) {
      (window as any).toastr.options.positionClass = 'toast-bottom-right';
      (window as any).toastr.success('Вы успешно вошли');
    }
  },error=>{
    (window as any).toastr.options.positionClass = 'toast-bottom-right';
    (window as any).toastr.error('Неверный логин или пароль');
  });
}

 resetPassword() {
  if (this.reset_email) {
    this.service.forgotPassword(new ForgotPasswordModel(this.reset_email)).subscribe();
  } else {
    alert('Введите email для сброса');
  }
}
redirectToRegister(){
  this.router.navigate(['register']);
}
}

