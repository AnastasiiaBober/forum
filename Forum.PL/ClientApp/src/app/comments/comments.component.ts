import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CommentModel, EventModel, PhotoModel, PostModel } from '../models';
import { UserModel } from '../models/user.model';
import { AuthorizationService } from '../services/authorization.service';
import { CommentsService } from '../services/comments.service';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ["./comments.component.css"]
})
export class CommentsComponent implements OnInit {
  public allComments: CommentModel[] = [];
  public commentsDidLoaded = false;
  public postDidLoaded = false;
  public pages = [];
  public photo1: string;
  public photo2: string;
  public photo3: string;
  postId: string;
  parentId: number;
  public post: PostModel;
  public description: string;
  isAuthorized = false;
  userDetails: any;
  isAdmin = false;
  constructor(private commentService: CommentsService, private authService: AuthorizationService, private postService: PostsService, private route: ActivatedRoute) {
    this.parentId = 0;
  }
  ngOnInit() {
    this.userDetails = new UserModel();
    this.userDetails.id = "";
    this.authService.isAuthorized().subscribe(x => {
      this.isAuthorized = x;
      if (this.isAuthorized) {
        this.authService.getDetails().subscribe((userDetails: UserModel) => {
          this.userDetails = userDetails;
        });
        this.authService.checkAdminRole().subscribe(x => this.isAdmin = x);
      }
    });
    this.postId = this.route.snapshot.paramMap.get('id');
    this.postService.getPost(this.postId).subscribe(resp => {
      this.post = resp;
      this.postDidLoaded = true
    })
    this.commentService.getCommentsCount(this.postId).subscribe((res: number) => {
      this.pages = res > 0 ? new Array(Math.ceil(res / 6)) : new Array(1);
    })
    this.commentService.getComments(this.postId, 0).subscribe(resp => {
      this.allComments = resp;
      this.commentsDidLoaded = true;
    })
  }
  changePage(i: number) {
    this.commentService.getComments(this.postId, i).subscribe(res => {
      this.allComments = res;
      this.commentsDidLoaded = true;
    })
  }
  onAddComment() {
    var comment = new CommentModel();
    comment.content = this.description;
    comment.postId = parseInt(this.postId);
    comment.photos = new Array<PhotoModel>();
    comment.parentId = this.parentId == 0 ? null : this.parentId;
    if (this.photo1) {
      comment.photos.push(new PhotoModel(this.photo1));
    }
    if (this.photo2) {
      comment.photos.push(new PhotoModel(this.photo2));
    }
    if (this.photo3) {
      comment.photos.push(new PhotoModel(this.photo3));
    }
    this.commentService.sendComment(comment).subscribe((res: CommentModel) => {
      this.description = '';
      (document.getElementById('photo1') as HTMLInputElement).value = '';
      (document.getElementById('photo2') as HTMLInputElement).value = '';
      (document.getElementById('photo3') as HTMLInputElement).value = '';
      this.allComments.unshift(res);
    })
  }
  handleFileUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      if (event.target.id === "photo1") {
        this.photo1 = reader.result as string;
      }
      else if (event.target.id === "photo2") {
        this.photo2 = reader.result as string;
      }
      else {
        this.photo3 = reader.result as string;
      }
    };
  }
  changeParentId(id: number) {
    this.parentId = id;
  }
  deleteComment(id: number) {
    this.commentService.removeComment(id).subscribe(() => {
      this.allComments = this.allComments.filter(x => x.id != id);
    })
  }
}
