import { Component } from '@angular/core';
import { AuthorizationService } from '../services/authorization.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { RegisterModel } from '../models';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html'
})
export class RegisterComponent {
  profileForm: FormGroup;
  constructor(private service : AuthorizationService){
    this.profileForm = new FormGroup({
      name: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [
        Validators.required,
        Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d).{8,}$')
      ]),
      phoneNumber: new FormControl('', [
        Validators.required, Validators.pattern('^\\+?3?8?(0\\d{9})$')
      ])
    });
  }

  register(){
    var model = new RegisterModel(this.profileForm.controls['email'].value,
    this.profileForm.controls['password'].value,
    this.profileForm.controls['name'].value,
    this.profileForm.controls['phoneNumber'].value);
    this.service.register(model).subscribe(resp=>{
      (window as any).toastr.options.positionClass = 'toast-bottom-right';
      (window as any).toastr.info('На вашу почту было отправлено письмо');
      (window as any).toastr.info('Подтвердите email');
    },error=>{
      (window as any).toastr.options.positionClass = 'toast-bottom-right';
      (window as any).toastr.error('Регистрация не прошла успешно');
      (window as any).toastr.info('Попробуйте ввести другое имя или пароль');
    })
  }
}
