import { Component, OnInit } from '@angular/core';
import { EventModel, PhotoModel } from '../models';
import { AuthorizationService } from '../services/authorization.service';
import { EventService } from '../services/events.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {
  public allEvents: EventModel[] = [];
  public eventsDidLoaded = false;
  public pages = [];
  public theme = '';
  public photo: string;
  public description = '';
  public isAdmin = false;
  constructor(private eventService: EventService, private authService: AuthorizationService) {
  }
  ngOnInit() {
    this.authService.isAuthorized().subscribe(x => {
      if (x) {
        this.authService.checkAdminRole().subscribe(x => this.isAdmin = x);
      }
    })
    this.eventService.getEventsCount().subscribe((res: number) => {
      this.pages = res > 0 ? new Array(Math.ceil(res / 6)) : new Array(1);
    })
    this.eventService.getEvents(0).subscribe(res => {
      this.allEvents = res;
      this.eventsDidLoaded = true;
    })
  }
  changePage(i: number) {
    this.eventService.getEvents(i).subscribe(res => {
      this.allEvents = res;
      this.eventsDidLoaded = true;
    })
  }
  onAddEvent() {
    var event = new EventModel();
    event.title = this.theme;
    event.content = this.description;
    event.photo = this.photo ? new PhotoModel(this.photo) : null;
    this.eventService.sendEvent(event).subscribe((res: EventModel) => {
      this.theme = '';
      this.description = '';
      (document.getElementById('photo') as HTMLInputElement).value = '';
      this.allEvents.unshift(res);
    })
  }
  handleFileUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.photo = reader.result as string;
    };
  }
  deleteEvent(id: number) {
    this.eventService.removeEvent(id).subscribe(() => {
      this.allEvents = this.allEvents.filter(x => x.id != id);
    });
  }
}
