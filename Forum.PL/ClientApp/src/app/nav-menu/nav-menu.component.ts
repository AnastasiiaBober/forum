import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from '../services/authorization.service';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './nav-menu.component.html',
  styleUrls: ['./nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {
  isAuthorized = false;
  constructor(private authService: AuthorizationService) {

  }
  ngOnInit(): void {
    this.authService.isAuthorized().subscribe(x => {
      this.isAuthorized = x;
    });
  }
  isExpanded = false;

  collapse() {
    this.isExpanded = false;
  }

  toggle() {
    this.isExpanded = !this.isExpanded;
  }
  logOut(){
    this.authService.logOut().subscribe(()=>{
      this.isAuthorized = false;
    })
  }
}
