import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { EventModel } from 'src/app/models';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  _baseUrl: string;
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl;
  }
  getEvents(offset: number) {
    return this.http.get<EventModel[]>(this._baseUrl + 'api/Events?offset=' + offset * 6);
  }
  getEventsCount() {
    return this.http.get<number>(this._baseUrl + 'api/Events/GetCount');
  }
  sendEvent(event: EventModel) {
    return this.http.post(this._baseUrl + 'api/Events', event);
  }
  removeEvent(eventId: number) {
    return this.http.delete(this._baseUrl + 'api/Events/' + eventId);
  }
}

