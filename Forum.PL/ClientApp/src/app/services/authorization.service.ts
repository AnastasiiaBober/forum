import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { LoginModel, ResetPasswordModel, RegisterModel, ForgotPasswordModel } from '../models';
import { UserModel } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  _baseUrl: string;
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl;
  }
  authorize(model: LoginModel) {
    return this.http.post(this._baseUrl + 'api/Accounts/Login', model, { observe: 'response' });
  }
  logOut() {
    return this.http.post(this._baseUrl + 'api/Accounts/Logout', null, { observe: 'response' });
  }
  resetPassword(model: ResetPasswordModel) {
    return this.http.post(this._baseUrl + 'api/Accounts/ResetPassword', model, { observe: 'response' })
  }
  register(model: RegisterModel) {
    return this.http.post(this._baseUrl + 'api/Accounts/Register', model,{ observe: 'response' });
  }
  forgotPassword(model: ForgotPasswordModel) {
    return this.http.post<string>(this._baseUrl + 'api/Accounts/ForgotPassword', model);
  }
  checkAdminRole(){
    return this.http.get<boolean>(this._baseUrl + 'api/Accounts/CheckAdminRole');
  }
  isAuthorized(){
    return this.http.get<boolean>(this._baseUrl + 'api/Accounts/Authorize');
  }
  getDetails(){
    return this.http.get<UserModel>(this._baseUrl + 'api/Accounts/GetDetails');
  }
}

