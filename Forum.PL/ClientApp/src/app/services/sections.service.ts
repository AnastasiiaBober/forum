import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { SectionModel } from '../models';

@Injectable({
  providedIn: 'root'
})
export class SectionsService {

  _baseUrl: string;
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl;
  }
  getAll(name: string) {
    return this.http.get<SectionModel[]>(this._baseUrl + 'api/Sections?name=' + name);
  }
  sendSection(model: SectionModel) {
    return this.http.post(this._baseUrl + 'api/Sections', model);
  }
  requestNewSection(model: SectionModel){
    return this.http.post(this._baseUrl + 'api/Sections/RequestNewSection', model);
  }
}
