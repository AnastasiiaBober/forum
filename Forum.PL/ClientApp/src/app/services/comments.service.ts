import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { CommentModel } from '../models';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  _baseUrl: string;
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl;
  }
  getComments(id: string, offset: number) {
    return this.http.get<CommentModel[]>(this._baseUrl + 'api/Comments?postId=' + id + '&offset=' + offset * 6);
  }
  sendComment(model: CommentModel) {
    return this.http.post(this._baseUrl + 'api/Comments', model);
  }
  getCommentsCount(id: string) {
    return this.http.get<number>(this._baseUrl + 'api/Comments/GetCount?postId=' + id);
  }
  removeComment(id: number) {
    return this.http.delete(this._baseUrl + 'api/Comments/' + id);
  }
}
