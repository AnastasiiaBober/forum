import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { PostModel, SectionModel } from '../models';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  _baseUrl: string;
  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this._baseUrl = baseUrl;
  }
  getPosts(id: string, offset: number) {
    return this.http.get<PostModel[]>(this._baseUrl + 'api/Posts?sectionId=' + id + '&offset=' + offset * 6);
  }
  getPost(id: string) {
    return this.http.get<PostModel>(this._baseUrl + 'api/Posts/' + id);
  }
  getPostsCount(id:string){
    return this.http.get<number>(this._baseUrl + 'api/Posts/GetCount?sectionId=' + id);
  }
  sendPost(post: PostModel) {
    return this.http.post(this._baseUrl + 'api/Posts',post);
  }
  removePost(postId: number) {
    return this.http.delete(this._baseUrl + 'api/Posts/' + postId);
  }
}
