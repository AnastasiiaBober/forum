import {Component} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { ResetPasswordModel } from '../models';
import { AuthorizationService } from '../services/authorization.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html'
})
export class ResetPasswordComponent {
  public code: string;
  public email: string;
  public password: string;

  constructor(private route: ActivatedRoute, private service: AuthorizationService, private router: Router) {
    this.route.queryParams.subscribe(params => {
      this.code = params['code'];
      this.email= params['email'];
    });
  }

  resetPassword() {
    this.service.resetPassword(new ResetPasswordModel(this.email, this.code, this.password)).subscribe(() => {
      (window as any).toastr.options.positionClass = 'toast-bottom-right';
      (window as any).toastr.success('Ваш пароль успешно восстановлен!');
      this.router.navigate(['login']);
    });
  }
}
