import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PhotoModel, PostModel } from '../models';
import { UserModel } from '../models/user.model';
import { AuthorizationService } from '../services/authorization.service';
import { PostsService } from '../services/posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html'
})
export class PostsComponent implements OnInit {
  public allPosts: PostModel[] = [];
  public postsDidLoaded = false;
  public pages = [];
  public theme = '';
  public adress = ''
  public photo: string;
  public description = '';
  public sectionId: string;
  public isAuthorized = false;
  public userDetails: UserModel;
  public isAdmin = false;
  constructor(private postsService: PostsService, private authService: AuthorizationService, private route: ActivatedRoute, private router: Router) {
  }
  ngOnInit() {
    this.userDetails = new UserModel();
    this.userDetails.id = "";
    this.authService.isAuthorized().subscribe(x => {
      this.isAuthorized = x;
      if (this.isAuthorized) {
        this.authService.getDetails().subscribe((userDetails: UserModel) => {
          this.userDetails = userDetails;
        })
        this.authService.checkAdminRole().subscribe(x => this.isAdmin = x);
      }
    });

    this.sectionId = this.route.snapshot.paramMap.get('id');
    this.postsService.getPostsCount(this.sectionId).subscribe((res: number) => {
      this.pages = res > 0 ? new Array(Math.ceil(res / 6)) : new Array(1);
    })
    this.postsService.getPosts(this.sectionId, 0).subscribe(resp => {
      this.allPosts = resp;
      this.postsDidLoaded = true;
    })
  }
  changePage(i: number) {
    this.postsService.getPosts(this.sectionId, i).subscribe(res => {
      this.allPosts = res;
      this.postsDidLoaded = true;
    })
  }
  getComments(id: number) {
    var route = 'posts/' + id;
    this.router.navigate([route]);
  }
  onAddEvent() {
    var newPost = new PostModel;
    newPost.address = this.adress;
    newPost.title = this.theme;
    newPost.content = this.description;
    newPost.sectionId = parseInt(this.sectionId);
    newPost.photo = this.photo ? new PhotoModel(this.photo) : null;
    this.postsService.sendPost(newPost).subscribe((res: PostModel) => {
      this.theme = '';
      this.adress = '';
      this.description = '';
      (document.getElementById('photo') as HTMLInputElement).value = '';
      this.allPosts.unshift(res);
    })
  }
  deletePost(id: number) {
    this.postsService.removePost(id).subscribe(() => {
      this.allPosts = this.allPosts.filter(x => x.id != id);
      this.postsService.getPostsCount(this.sectionId).subscribe((res: number) => {
        this.pages = res > 0 ? new Array(Math.ceil(res / 6)) : new Array(1);
      })
    });
  }
  handleFileUpload(event) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.photo = reader.result as string;
    };
  }
}
