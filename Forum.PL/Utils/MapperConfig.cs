﻿using AutoMapper;
using Forum.BLL.DTOs;
using Forum.BLL.Utils;
using Forum.PL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.PL.Utils
{
    public class MapperConfig : Profile
    {
        public MapperConfig()
        {
            CreateMap<EventDTO, EventViewModel>();
            CreateMap<PhotoDTO, PhotoViewModel>();
            CreateMap<EventViewModel, EventDTO>();
            CreateMap<PhotoViewModel, PhotoDTO>();
            CreateMap<SectionViewModel, SectionDTO>();
            CreateMap<SectionDTO, SectionViewModel>();
            CreateMap<PostDTO, PostViewModel>();
            CreateMap<PostViewModel, PostDTO>();
            CreateMap<CommentViewModel, CommentDTO>();
            CreateMap<CommentDTO, CommentViewModel>();
        }
    }
}
