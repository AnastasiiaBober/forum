﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.PL.Models
{
    public class PostViewModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public string Address { get; set; }
        public PhotoViewModel Photo { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int SectionId { get; set; }
        public ICollection<CommentViewModel> Comments { get; set; }
    }
}
