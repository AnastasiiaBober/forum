﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.DTOs;
using Forum.BLL.Services;
using Forum.PL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Forum.PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostsService _postsService;
        private readonly IMapper _mapper;
        public PostsController(IPostsService postsService, IMapper mapper)
        {
            _postsService = postsService;
            _mapper = mapper;
        }
        // GET: api/Posts
        [HttpGet]
        public IEnumerable<PostViewModel> Get(int sectionId, int offset = 0)
        {
            return _mapper.Map<IEnumerable<PostDTO>, IEnumerable<PostViewModel>>(_postsService.GetAll(offset, sectionId));
        }

        // GET: api/Posts/5
        [HttpGet("{id}")]
        public PostViewModel Get(int id)
        {
            return _mapper.Map<PostDTO, PostViewModel>(_postsService.Find(id));
        }

        [Authorize]
        [HttpPost]
        public PostViewModel Post([FromBody] PostViewModel model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            model.UserId = userId;
            return _mapper.Map<PostDTO, PostViewModel>(_postsService.Create(_mapper.Map<PostViewModel, PostDTO>(model)));
        }

        [Authorize]
        [HttpPut("{id}")]
        public void Put([FromBody] PostViewModel model)
        {
            _postsService.Create(_mapper.Map<PostViewModel, PostDTO>(model));
        }

        [Authorize]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _postsService.Delete(id);
        }

        [HttpGet("[action]")]
        public int GetCount(int sectionId)
        {
            return _postsService.GetAllCount(sectionId);
        }
    }
}
