﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Forum.BLL.DTOs;
using Forum.BLL.Services;
using Forum.PL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        readonly IEventsService _eventsService;
        private readonly IMapper _mapper;
        public EventsController(IEventsService eventsService, IMapper mapper)
        {
            _eventsService = eventsService;
            _mapper = mapper;
        }
        // GET: api/Events
        [HttpGet]
        public IEnumerable<EventViewModel> Get(int offset = 0)
        {
            return _mapper.Map<IEnumerable<EventDTO>, IEnumerable<EventViewModel>>(_eventsService.GetAll(offset));
        }

        // GET: api/Events/5
        [HttpGet("{id}")]
        public EventViewModel GetById(int id)
        {
            return _mapper.Map<EventDTO, EventViewModel>(_eventsService.Find(id));
        }

        [Authorize(Roles ="admin")]
        [HttpPost]
        public EventViewModel Post([FromBody] EventViewModel model)
        {
           return _mapper.Map<EventDTO, EventViewModel>(_eventsService.Create(_mapper.Map<EventViewModel, EventDTO>(model)));
        }

        [Authorize(Roles = "admin")]
        [HttpPut]
        public void Put([FromBody] EventViewModel model)
        {
            _eventsService.Update(_mapper.Map<EventViewModel, EventDTO>(model));
        }

        [Authorize(Roles = "admin")]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _eventsService.Delete(id);
        }
        [HttpGet("[action]")]
        public int GetCount()
        {
            return _eventsService.GetAllCount();
        }
    }
}
