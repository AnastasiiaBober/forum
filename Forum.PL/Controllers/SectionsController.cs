﻿using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.DTOs;
using Forum.BLL.Services;
using Forum.PL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SectionsController : ControllerBase
    {
        private readonly ISectionsService _sectionsService;
        private readonly IAccountsService _accountsService;
        private readonly IEmailService _emailService;
        private readonly IMapper _mapper;
        public SectionsController(ISectionsService sectionsService, IMapper mapper, IAccountsService accountsService, IEmailService emailService)
        {
            _sectionsService = sectionsService;
            _mapper = mapper;
            _accountsService = accountsService;
            _emailService = emailService;
        }
        // GET: api/Sections
        [HttpGet]
        public IEnumerable<SectionViewModel> Get(string name = "")
        {
            return _mapper.Map<IEnumerable<SectionDTO>, IEnumerable<SectionViewModel>>(_sectionsService.GetAll(name));
        }

        // GET: api/Sections/5
        [HttpGet("{id}")]
        public SectionViewModel Get(int id)
        {
            return _mapper.Map<SectionDTO, SectionViewModel>(_sectionsService.Find(id));
        }

        [HttpPost]
        [Authorize(Roles = "admin")]
        public SectionViewModel Post([FromBody] SectionViewModel model)
        {
            return _mapper.Map<SectionDTO, SectionViewModel>(_sectionsService.Create(_mapper.Map<SectionViewModel, SectionDTO>(model)));
        }

        // PUT: api/Sections/5
        [HttpPut]
        [Authorize(Roles = "admin")]
        public void Put([FromBody] SectionViewModel model)
        {
            _sectionsService.Update(_mapper.Map<SectionViewModel, SectionDTO>(model));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        [Authorize(Roles = "admin")]
        public void Delete(int id)
        {
            _sectionsService.Delete(id);
        }
        [HttpPost("[action]")]
        [Authorize]
        public async Task<ActionResult> RequestNewSection([FromBody] SectionViewModel model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _accountsService.GetUserByIdAsync(userId);
            await _emailService.SendEmailAsync("zakladukyiv@gmail.com", "Запрос на создание раздела",
                $"Пользователь {user.Email} предлагает добавить раздел с название {model.Name}");
            return Ok();
        }
    }
}
