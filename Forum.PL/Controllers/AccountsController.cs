﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Forum.BLL.DTOs;
using Forum.BLL.Services;
using Forum.PL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Forum.PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountsController : ControllerBase
    {
        private readonly IAccountsService _accountService;
        private readonly IEmailService _emailService;
        public AccountsController(IAccountsService accountService, IEmailService emailService)
        {
            _accountService = accountService;
            _emailService = emailService;
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel model)
        {
            RegisterModelDTO dto = new RegisterModelDTO
            {
                Name = model.Name,
                Login = model.Login,
                Number = model.Number,
                Password = model.Password
            };
            var confiramationModel = new ConfirmationModel();
            var result = await _accountService.AddAccount(dto, confiramationModel);
            if (!result.Succeeded) return BadRequest();
            var callbackUrl = Url.Action(
                    "ConfirmEmail",
                    "Accounts",
                    new { userId = confiramationModel.UserId, code = confiramationModel.Code },
                    protocol: HttpContext.Request.Scheme);
            await _emailService.SendEmailAsync(model.Login, "Подтвердите ваш аккаунт",
                $"Подтвердите регистрацию, перейдя по ссылке: <a href='{callbackUrl}'>link</a>");
            return Ok();
        }
        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return Forbid();
            }
            var model = new ConfirmationModel() { Code = code, UserId = userId };
            var result = await _accountService.ConfirmEmail(model);
            if (result.Succeeded)
                return Redirect("https://localhost:44395/login");
            return BadRequest();
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> Login([FromBody] LoginViewModel model)
        {
            LoginModelDTO modelDTO = new LoginModelDTO
            {
                Name = model.Name,
                Password = model.Password
            };
            var result = await _accountService.Authorize(modelDTO);
            if (result.Succeeded)
            {
                return Ok();
            }
            return BadRequest();
        }
        [HttpPost("[action]")]
        public async Task<ActionResult<string>> ForgotPassword(ForgotPasswordViewModel model)
        {
            var confirmationModel = await _accountService.ForgotPassword(model.Email);
            if (confirmationModel == null) return Forbid();
            var callbackUrl = Url.Action("ResetPassword", "Accounts", new { userId = confirmationModel.UserId, code = confirmationModel.Code },
                protocol: HttpContext.Request.Scheme);
            await _emailService.SendEmailAsync(model.Email, "Сброс пароля",
            $"Здравствуйте, {model.Email}. Для сброса пароля пройдите по ссылке: <a href='{callbackUrl}'>link</a>");
            return Ok();
        }

        [HttpGet("[action]")]
        public async Task<IActionResult> ResetPassword(string userId, string code)
        {
            if (string.IsNullOrEmpty(userId) || string.IsNullOrEmpty(code))
            {
                return Forbid();
            }
            var user = await _accountService.GetUserByIdAsync(userId);
            return string.IsNullOrEmpty(user.Email) ? BadRequest() : (IActionResult)Redirect($"https://localhost:44395/reset?code={code}&email={user.Email}");
        }

        [HttpPost("[action]")]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            ResetPasswordModelDTO resetPasswordModel = new ResetPasswordModelDTO
            {
                Code = model.Code,
                Email = model.Email,
                Password = model.Password
            };
            var result = await _accountService.ResetPassword(resetPasswordModel);
            if (result.Succeeded)
                return Ok();
            return BadRequest();
        }
        [Authorize]
        [HttpGet("[action]")]
        public async Task<ActionResult<bool>> CheckAdminRole()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var role = await _accountService.GetUserRole(userId);
            var result = !string.IsNullOrWhiteSpace(role) && role == "admin";
            return Ok(result);
        }
        [Authorize]
        [HttpPost("{id}")]
        public async Task<IActionResult> AddUserToAdminRole(string id)
        {
            await _accountService.AddUserToAdminRole(id);
            return Ok();
        }
        [HttpGet("[action]")]
        public ActionResult<bool> Authorize() => Ok(User.Identity.IsAuthenticated);

        [Authorize]
        [HttpGet("[action]")]
        public async Task<ActionResult<UserViewModel>> GetDetails()
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            var user = await _accountService.GetUserByIdAsync(userId);
            var model = new UserViewModel()
            {
                Email = user.Email,
                UserName = user.UserName,
                Id = user.Id
            };
            return Ok(model);
        }
        [Authorize]
        [HttpPost("[action]")]
        public async Task<ActionResult> Logout()
        {
            await _accountService.LogOut();
            return Ok();
        }
    }
}
