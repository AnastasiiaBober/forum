﻿using System.Collections.Generic;
using System.Security.Claims;
using AutoMapper;
using Forum.BLL.DTOs;
using Forum.BLL.Services;
using Forum.PL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Forum.PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        readonly ICommentsService _commentsService;
        private readonly IMapper _mapper;
        public CommentsController(ICommentsService commentsService, IMapper mapper)
        {
            _commentsService = commentsService;
            _mapper = mapper;
        }

        [HttpGet]
        public IEnumerable<CommentViewModel> Get(int postId, int offset = 0)
        {
            return _mapper.Map<IEnumerable<CommentDTO>, IEnumerable<CommentViewModel>>(_commentsService.GetAll(offset, postId));
        }

        [HttpGet("{id}")]
        public CommentViewModel GetById(int id)
        {
            return _mapper.Map<CommentDTO, CommentViewModel>(_commentsService.Find(id));
        }
        [Authorize]
        [HttpPost]
        public CommentViewModel Post([FromBody] CommentViewModel model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            model.UserId = userId;
            return _mapper.Map<CommentDTO, CommentViewModel>(_commentsService.Create(_mapper.Map<CommentViewModel, CommentDTO>(model)));
        }
        [Authorize]
        [HttpPut]
        public void Put([FromBody] CommentViewModel model)
        {
            _commentsService.Update(_mapper.Map<CommentViewModel, CommentDTO>(model));
        }
        [Authorize]
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _commentsService.Delete(id);
        }

        [HttpGet("[action]")]
        public int GetCount(int postId)
        {
            return _commentsService.GetAllCount(postId);
        }
    }
}
