﻿using AutoMapper;
using Forum.BLL.DTOs;
using Forum.DAL.Models;
using Forum.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.Services
{
    public interface IEventsService
    {
        IEnumerable<EventDTO> GetAll(int offset);
        int GetAllCount();
        EventDTO Create(EventDTO eventDTO);
        EventDTO Find(int id);
        void Delete(int id);
        void Update(EventDTO eventDTO);
    }
    public class EventsService : IEventsService
    {
        private readonly IUnitOfWork _uow;
        public EventsService(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public IEnumerable<EventDTO> GetAll(int offset)
        {
            List<EventDTO> list = new List<EventDTO>();
            var events = _uow.EventRepository.GetFiltered(offset);
            foreach (var item in events)
            {
                list.Add(new EventDTO { Id = item.Id, Photo = new PhotoDTO() { Code = item?.Photo?.Code }, Content = item.Content, Date = item.Date, Title = item.Title });
            }
            return list;
        }
        public int GetAllCount()
        {
            return _uow.EventRepository.GetFilteredCount();
        }
        public EventDTO Create(EventDTO eventDTO)
        {
            var model = new Event()
            {
                Id = eventDTO.Id,
                Photo = new Photo() { Code = eventDTO?.Photo?.Code },
                Content = eventDTO.Content,
                Date = eventDTO.Date,
                Title = eventDTO.Title
            };
           var modelFromDB =  _uow.EventRepository.Create(model);
            _uow.Save();
            var dto = new EventDTO()
            {
                Id = modelFromDB.Id,
                Photo = new PhotoDTO() { Code = model?.Photo?.Code },
                Content = modelFromDB.Content,
                Date = modelFromDB.Date,
                Title = modelFromDB.Title,
            };
            return dto;
        }
        public EventDTO Find(int id)
        {
            var model = _uow.EventRepository.Get(id);
            var dto = new EventDTO()
            {
                Id = model.Id,
                Photo = new PhotoDTO() { Code = model.Photo.Code },
                Content = model.Content,
                Date = model.Date,
                Title = model.Title
            };
            return dto;
        }

        public void Delete(int id)
        {
            _uow.EventRepository.Delete(id);
            _uow.Save();
        }
        public void Update(EventDTO eventDTO)
        {
            var model = new Event()
            {
                Id = eventDTO.Id,
                Photo = new Photo() { Code = eventDTO.Photo.Code },
                Content = eventDTO.Content,
                Date = eventDTO.Date,
                Title = eventDTO.Title
            };
            _uow.EventRepository.Update(model);
            _uow.Save();
        }
    }
}
