﻿using Forum.BLL.DTOs;
using Forum.DAL.Models;
using Forum.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.Services
{
    public interface IPostsService
    {
        IEnumerable<PostDTO> GetAll(int offset, int sectionId);
        int GetAllCount(int sectionId);
        PostDTO Create(PostDTO postDTO);
        PostDTO Find(int id);
        void Delete(int id);
        void Update(PostDTO postDTO);
    }
    public class PostsService : IPostsService
    {
        readonly IUnitOfWork _uow;
        public PostsService(IUnitOfWork ouw)
        {
            _uow = ouw;
        }
        public PostDTO Create(PostDTO postDTO)
        {
            var model = new Post
            {
                Id = postDTO.Id,
                Date = postDTO.Date,
                Address = postDTO.Address,
                Content = postDTO.Content,
                SectionId = postDTO.SectionId,
                Title = postDTO.Title,
                UserId = postDTO.UserId
            };
            model.Photo = postDTO.Photo != null ? new Photo { Code = postDTO.Photo.Code } : null;
            _uow.PostRepository.Create(model);
            _uow.Save();
            var modelFromDB = _uow.PostRepository.Get(model.Id);
            var dto = new PostDTO()
            {
                Id = modelFromDB.Id,
                Date = modelFromDB.Date,
                Photo = new PhotoDTO { Code = model?.Photo?.Code },
                Address = modelFromDB.Address,
                Comments = modelFromDB.Comments,
                Content = modelFromDB.Content,
                Title = modelFromDB.Title,
                UserId = modelFromDB.UserId,
                UserName = modelFromDB.User.UserName
            };
            return dto;
        }

        public void Delete(int id)
        {
            _uow.PostRepository.Delete(id);
            _uow.Save();
        }

        public PostDTO Find(int id)
        {
            var model = _uow.PostRepository.Get(id);
            var dto = new PostDTO()
            {
                Id = model.Id,
                Date = model.Date,
                Photo = new PhotoDTO { Code = model?.Photo?.Code },
                Address = model.Address,
                Comments = model.Comments,
                Content = model.Content,
                Title = model.Title,
                UserName = model?.User?.UserName
            };
            return dto;
        }

        public IEnumerable<PostDTO> GetAll(int offset, int sectionId)
        {
            List<PostDTO> list = new List<PostDTO>();
            var posts = _uow.PostRepository.GetFiltered(offset, sectionId);
            foreach (var item in posts)
            {
                list.Add(new PostDTO
                {
                    Id = item.Id,
                    Photo = new PhotoDTO() { Code = item?.Photo?.Code },
                    Content = item.Content,
                    Date = item.Date,
                    Title = item.Title,
                    Address = item.Address,
                    UserId = item.UserId,
                    UserName = item?.User?.UserName
                });

            }
            return list;
        }
        public int GetAllCount(int sectionId)
        {
            return _uow.PostRepository.GetFilteredCount(sectionId);
        }

        public void Update(PostDTO postDTO)
        {
            var model = new Post
            {
                Id = postDTO.Id,
                Date = postDTO.Date,
                Address = postDTO.Address,
                Content = postDTO.Content,
                Photo = new Photo { Code = postDTO.Photo.Code },
                SectionId = postDTO.SectionId,
                Title = postDTO.Title,
                UserId = postDTO.UserId
            };
            _uow.PostRepository.Update(model);
            _uow.Save();
        }
    }
}
