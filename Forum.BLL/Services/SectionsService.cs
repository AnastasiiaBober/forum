﻿using Forum.BLL.DTOs;
using Forum.DAL.Models;
using Forum.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.Services
{
    public interface ISectionsService
    {
        IEnumerable<SectionDTO> GetAll(string name);
        SectionDTO Create(SectionDTO eventDTO);
        SectionDTO Find(int id);
        void Delete(int id);
        void Update(SectionDTO eventDTO);
    }
    public class SectionsService : ISectionsService
    {
        readonly IUnitOfWork _uow;
        public SectionsService(IUnitOfWork ouw)
        {
            _uow = ouw;
        }
        public SectionDTO Create(SectionDTO eventDTO)
        {
            var model = new Section
            {
                Id = eventDTO.Id,
                Date = eventDTO.Date,
                Name = eventDTO.Name,
                Posts = eventDTO.Posts
            };
            var newSection = _uow.SectionRepository.Create(model);
            _uow.Save();
            var dto = new SectionDTO()
            {
                Id = newSection.Id,
                Date = newSection.Date,
                Name = newSection.Name,
            };
            return dto;
        }

        public void Delete(int id)
        {
            _uow.SectionRepository.Delete(id);
            _uow.Save();
        }

        public SectionDTO Find(int id)
        {
            var model = _uow.SectionRepository.Get(id);
            var dto = new SectionDTO()
            {
                Id = model.Id,
                Date = model.Date,
                Name = model.Name,
                Posts = model.Posts
            };
            return dto;
        }

        public IEnumerable<SectionDTO> GetAll(string name)
        {
            List<SectionDTO> list = new List<SectionDTO>();
            var sections = _uow.SectionRepository.GetFilteredByName(name);
            foreach (var item in sections)
            {
                list.Add(new SectionDTO { Id = item.Id, Date = item.Date, Name = item.Name });
            }
            return list;
        }

        public void Update(SectionDTO eventDTO)
        {
            var model = new Section
            {
                Id = eventDTO.Id,
                Date = eventDTO.Date,
                Name = eventDTO.Name,
            };
            _uow.SectionRepository.Update(model);
            _uow.Save();
        }
    }
}
