﻿using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Threading.Tasks;

namespace Forum.BLL.Services
{
    public interface IEmailService
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
    public class EmailService : IEmailService
    {
        public async Task SendEmailAsync(string email, string subject, string message)
        {
            var emailMessage = new MimeMessage();

            emailMessage.From.Add(new MailboxAddress("ZakladuKyiv Administrator", "zakladukyiv@gmail.com"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;
            emailMessage.Body = new TextPart(MimeKit.Text.TextFormat.Html)
            {
                Text = message
            };
            try
            {
                using var client = new SmtpClient();
                client.CheckCertificateRevocation = false;
                await client.ConnectAsync("smtp.gmail.com", 465, true);
                await client.AuthenticateAsync("zakladukyiv@gmail.com", "mlgtnqqbkxmiyljt");
                await client.SendAsync(emailMessage);

                await client.DisconnectAsync(true);
            }
            catch (Exception e)
            {
               // Log.Logger.Information("Exception from gmail:" + e);
                throw;
            }

        }
    }
}
