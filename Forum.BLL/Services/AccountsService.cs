﻿using Forum.BLL.DTOs;
using Forum.DAL.Models;
using Microsoft.AspNetCore.Identity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BLL.Services
{
    public interface IAccountsService
    {
        Task<IdentityResult> AddAccount(RegisterModelDTO model, ConfirmationModel confirmationModel);
        Task<SignInResult> Authorize(LoginModelDTO model);
        Task LogOut();
        Task<IdentityResult> ConfirmEmail(ConfirmationModel model);
        Task<IdentityResult> ResetPassword(ResetPasswordModelDTO model);
        Task<ConfirmationModel> ForgotPassword(string email);
        Task<string> GetUserRole(string userName);
        Task<User> GetUserByIdAsync(string id);
        Task AddUserToAdminRole(string id);
    }
    public class AccountsService : IAccountsService
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public AccountsService(UserManager<User> userManager, RoleManager<IdentityRole> roleManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
        }
        public async Task<IdentityResult> AddAccount(RegisterModelDTO model, ConfirmationModel confirmationModel)
        {
            User user = new User { Email = model.Login, UserName = model.Name, PhoneNumber = model.Number };
            var result = await _userManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                confirmationModel.UserId = user.Id;
                confirmationModel.Code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            }
            return result;
        }

        public async Task<SignInResult> Authorize(LoginModelDTO model)
        {
            var user = await _userManager.FindByNameAsync(model.Name);
            SignInResult result = new SignInResult();
            if (user == null || !await _userManager.IsEmailConfirmedAsync(user))
            {
                return result;
            }
            return await _signInManager.PasswordSignInAsync(model.Name, model.Password, true, false);
        }

        public Task LogOut()
        {
            return _signInManager.SignOutAsync();
        }

        public async Task<IdentityResult> ConfirmEmail(ConfirmationModel model)
        {
            var result = new IdentityResult();
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user != null)
                result = await _userManager.ConfirmEmailAsync(user, model.Code);
            return result;
        }
        public async Task<IdentityResult> ResetPassword(ResetPasswordModelDTO model)
        {
            var result = new IdentityResult();
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null)
            {
                model.Code = model.Code.Replace(' ', '+');
                result = await _userManager.ResetPasswordAsync(user, model.Code, model.Password);
            }
            return result;
        }
        public async Task<ConfirmationModel> ForgotPassword(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null || !(await _userManager.IsEmailConfirmedAsync(user)))
            {
                return null;
            }
            var code = await _userManager.GeneratePasswordResetTokenAsync(user);
            var model = new ConfirmationModel
            {
                Code = code,
                UserId = user.Id
            };
            return model;
        }
        public async Task<User> GetUserByIdAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            return user;
        }
        public async Task AddUserToAdminRole(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            var userRoles = await _userManager.GetRolesAsync(user);

            var roles = _roleManager.Roles.ToList();
            var rolesNames = roles.Select(x => x.Name);
            if (!rolesNames.Contains("admin"))
            {
                await _roleManager.CreateAsync(new IdentityRole("admin"));
            }
            if (user != null && !userRoles.Contains("admin"))
            {
                await _userManager.AddToRoleAsync(user, "admin");
            }
        }
        public async Task<string> GetUserRole(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if(user == null)
            {
                return null;
            }
            var roles = await _userManager.GetRolesAsync(user);
            return roles.FirstOrDefault();
        }
    }
}
