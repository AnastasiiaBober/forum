﻿using Forum.BLL.DTOs;
using Forum.DAL.Models;
using Forum.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Forum.BLL.Services
{
    public interface ICommentsService
    {
        IEnumerable<CommentDTO> GetAll(int offset, int postId);
        int GetAllCount(int postId);
        CommentDTO Create(CommentDTO commentDTO);
        CommentDTO Find(int id);
        void Delete(int id);
        void Update(CommentDTO commentDTO);
    }
    public class CommentsService : ICommentsService
    {
        readonly IUnitOfWork _uow;
        public CommentsService(IUnitOfWork ouw)
        {
            _uow = ouw;
        }
        public CommentDTO Create(CommentDTO commentDTO)
        {
            var model = new Comment
            {
                Id = commentDTO.Id,
                Date = commentDTO.Date,
                Content = commentDTO.Content,
                ParentId = commentDTO.ParentId,
                PostId = commentDTO.PostId,
                UserId = commentDTO.UserId,
            };
            if (commentDTO.Photos != null && commentDTO.Photos.Any())
            {
                model.Photos = commentDTO.Photos.Select(x => new Photo() { Code = x.Code }).ToList();
            }
             _uow.CommentRepository.Create(model);
            _uow.Save();
            var modelFromDB = _uow.CommentRepository.Get(model.Id);
            var dto = new CommentDTO()
            {
                Id = modelFromDB.Id,
                Date = modelFromDB.Date,
                Content = modelFromDB.Content,
                PostId = modelFromDB.PostId,
                UserId = modelFromDB.UserId,
                ParentId = modelFromDB.ParentId,
                UserName = modelFromDB.User.UserName,
                Photos = modelFromDB.Photos?.Select(photo => new PhotoDTO { Code = photo?.Code }).ToList(),
            };
            if (modelFromDB.ParentId != null)
            {
                var parent = _uow.CommentRepository.Get(modelFromDB.ParentId.Value);
                dto.Parent = new CommentDTO
                {
                    Id = parent.Id,
                    Content = parent.Content,
                    Date = parent.Date,
                    UserName = parent?.User?.UserName
                };
            }
            return dto;
        }

        public void Delete(int id)
        {
            _uow.CommentRepository.Delete(id);
            _uow.Save();
        }

        public CommentDTO Find(int id)
        {
            var model = _uow.CommentRepository.Get(id);
            var dto = new CommentDTO()
            {
                Id = model.Id,
                Date = model.Date,
                Content = model.Content,
                PostId = model.PostId,

            };
            return dto;
        }

        public IEnumerable<CommentDTO> GetAll(int offset, int postId)
        {
            var posts = _uow.CommentRepository.GetFiltered(offset, postId);
            var list = posts.Select(x => new CommentDTO
            {
                Content = x.Content,
                Date = x.Date,
                Id = x.Id,
                ParentId = x.ParentId,
                PostId = x.PostId,
                UserId = x.UserId,
                UserName = x?.User?.UserName,
                Photos = x.Photos?.Select(photo => new PhotoDTO { Code = photo?.Code }).ToList(),
                Parent = x.Parent == null ? null :
                 new CommentDTO
                 {
                     Id = x.Parent.Id,
                     Content = x.Parent.Content,
                     Photos = x.Parent?.Photos?.Select(photo => new PhotoDTO { Code = photo?.Code }).ToList(),
                     Date = x.Parent.Date,
                     UserName = x?.Parent?.User?.UserName
                 }
            }).ToList();
            return list;
        }
        public int GetAllCount(int postId)
        {
            return _uow.CommentRepository.GetFilteredCount(postId);
        }

        public void Update(CommentDTO commentDTO)
        {
            var model = new Comment
            {
                Id = commentDTO.Id,
                Date = commentDTO.Date,
                Content = commentDTO.Content,
                Photos = commentDTO.Photos?.Select(x => new Photo() { Code = x.Code }).ToList()
            };
            _uow.CommentRepository.Update(model);
            _uow.Save();
        }
    }
}
