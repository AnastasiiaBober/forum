﻿using Forum.DAL.Models;
using Forum.DAL.UOW;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Forum.BLL.Utils
{
    public static class ServiceCollectionExtensions
    {
        public static void AddApplicationDependencies(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<ForumContext>(options =>
            options.UseSqlServer(connectionString));
            services.AddIdentity<User, IdentityRole>(o=>
            {
                o.Password.RequireNonAlphanumeric = false;
            }).AddEntityFrameworkStores<ForumContext>()
              .AddDefaultTokenProviders();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
        }
    }
}
