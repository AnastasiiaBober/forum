﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.DTOs
{
    public class ResetPasswordModelDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Code { get; set; }
    }
}
