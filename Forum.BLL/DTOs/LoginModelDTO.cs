﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.DTOs
{
    public class LoginModelDTO
    {
        public string Name { get; set; }
        public string Password { get; set; }
    }
}
