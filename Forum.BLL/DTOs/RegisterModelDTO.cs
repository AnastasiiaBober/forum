﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.DTOs
{
    public class RegisterModelDTO
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string Number { get; set; }
    }
}
