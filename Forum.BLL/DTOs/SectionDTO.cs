﻿using Forum.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.DTOs
{
    public class SectionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public ICollection<Post> Posts { get; set; }
    }
}
