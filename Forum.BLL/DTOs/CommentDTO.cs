﻿using Forum.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.DTOs
{
    public class CommentDTO
    {
        public int Id { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public int? ParentId { get; set; }
        public int PostId { get; set; }
        public CommentDTO Parent { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public ICollection<PhotoDTO> Photos { get; set; }
    }
}
