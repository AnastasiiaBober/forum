﻿using Forum.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.DTOs
{
    public class PostDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public string Address { get; set; }
        public PhotoDTO Photo { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public int SectionId { get; set; }
        public ICollection<Comment> Comments { get; set; }
    }
}
