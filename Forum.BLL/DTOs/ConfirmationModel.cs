﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.DTOs
{
    public class ConfirmationModel
    {
        public string UserId { get; set; }
        public string Code { get; set; }
    }
}
