﻿using Forum.DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Forum.BLL.DTOs
{
    public class EventDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public int PhotoId { get; set; }
        public PhotoDTO Photo { get; set; }
    }
}
